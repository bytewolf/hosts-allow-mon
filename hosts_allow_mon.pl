#!/usr/bin/env perl
# hui
# hosts_allow_mon.pl for baoz
# monitor hosts.allow files with specified ip.txt,
# and it sends mail for alerts if unexpected ip in hosts.allow has been detected.
# ------------------------------
# Requirement: Perl5, sendmail

use strict;
use POSIX qw/strftime tmpnam/;
use Getopt::Long;



my $send = "";
my $recv_addr = '';
my $send_addr = '';
my $smtp_serv = '';
my $smtp_user = "";
my $smtp_pass = "";
my $SENDMAIL = 'sendEmail_155';
my $tmp_file = tmpnam()."hosts_allow_mon".$$;
chomp $SENDMAIL;
die "No sendEmail found!\n" if ( !(-e $SENDMAIL) && $send);
my ($ips, $dir, %ips, %alerts);
GetOptions(
       'ips=s' => \$ips,
       'dir=s' => \$dir,
       'recv=s' => \$recv_addr,
       'send' => \$send,
       'help' => sub{ goto USAGE },
) or die "$!\n";
$dir .= '/' unless $dir =~ /\/$/;

USAGE:
die<<"USAGE" unless ( $ips && $dir );

Usage:
       perl $0 --ips ip.txt --dir /hostnames/ --recv watch\@etfiber.com --send

       --ips    Expected ip list file.
       --dir    Top directory contains hostname directories.
       --recv   Mail recipient.
       --send   Send alert to recipient, otherwise just print alert messge on the screen.

USAGE

if( -e $ips ){
       open(F, $ips) or die "Can\'t open ip list file:$ips\n";
       map { chomp; $ips{$_} = 1 } (<F>);
       close F;
}else{
       die "$ips doesn\'t exists!";
}

if( -d $dir ){
       opendir(DIR, $dir) or die " Can\'t change dir to $dir !\n";
       my @dirs = grep{ !/^\./ && -d "${dir}$_" } readdir(DIR);
       close DIR;

       for my $host (@dirs){
               my $hosts_allow = "${dir}$host/hosts.allow";
               my $hosts_deny = "${dir}$host/hosts.deny";

               unless( -s $hosts_deny ){
                        $alerts{$host}->{deny_not_exists} = 1;
               };
               unless( -s $hosts_allow ){
                        $alerts{$host}->{allow_not_exists} = 1;
                        next;          
               };
               
               if(-e $hosts_deny){
                        local $/ = undef;
                        open(DENY,$hosts_deny) or warn "Can\'t open $hosts_deny: $!\n";
                        my $content = <DENY>;
                        close DENY;
                        $alerts{$host}->{deny_all} = 1 unless $content =~ /^\s*?all\:all(?:#|$)/im;
               
               }

               if( -e $hosts_allow){
                       local $/ = undef;
                       open(H,"$hosts_allow") or warn "Can\'t open $hosts_allow: $! \n";
                       my $content = <H>;
                       close H;
                       &Check_hosts($host, \$content);
               }else{
                       warn "File $hosts_allow doesn\'t exists!\n";
               }
       }
}else{
       die "$dir doesn\'t exists!\n";
}

###### report
my $risk_hosts = "";
my $risk_details ="";
$risk_hosts= join(", ",keys %alerts);

for my $host ( sort { $alerts{$a}->{allow_not_exists} <=> $alerts{$b}->{allow_not_exists} } keys %alerts){
        my $border = "="x(length($host)+7);
        my $flag = int(!$alerts{$host}->{deny_not_exists}).int(!$alerts{$host}->{allow_not_exists});
        $risk_details.= "$host ";

        if( $alerts{$host}->{unexpected_ip} ){
                 
                 $risk_details.= "Abnormal IP: ".join(", ",@{$alerts{$host}->{unexpected_ip}} );
                 #$risk_details.= "Abnormal IP: ".join(", ",@{$alerts{$host}->{unexpected_ip}})."\n";

        }

        if( $alerts{$host}->{deny_not_exists} ){
                $risk_details .= "hosts.deny not exist! ";
        }

        if( $alerts{$host}->{allow_not_exists} ){
                $risk_details .= "hosts.allow not exist!\n";
                next;
        }


        $risk_details.= "hosts.allow ALL open! " if $alerts{$host}->{allow_all};
        $risk_details.= "hosts.deny ALL closed!\n" if $alerts{$host}->{deny_all};
        $risk_details.="\n";
}

my $time = strftime("%Y-%m-%d %H:%M:%S",localtime());
my $msg = <<"EOF";
$risk_details

EOF


###### print and send
Send_alert($msg);
#Send_alert("[host allow/deny alert!!!] $time",$msg);


###### sub
sub Check_hosts {
       my $host = $_[0];
       my $content = ${$_[1]};
       my $ip_string = "";
       my $msg = "";
       my $unexpected_ips = "";

       while($content =~ m/^\s*sshd:(.+?)(?:#|$)/gim){
               $ip_string.= "$1 ";
               $alerts{$host}->{allow_all} = 1 if $ip_string =~ s/all//gim;
       }
       $ip_string =~ s/\d+\/[\d\.]+\s/ /g;
       my @current_ips = split(/\s+/,$ip_string);

       my $expected_ips = join(" ",keys %ips);

          for my $current_ip (@current_ips){
                  next if $current_ip =~ /^(?:172|10|192|127)\b/;
				  $current_ip =~ s/\.\d{1,3}$// if $current_ip =~ /^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$/;
                  push @{ $alerts{$host}->{unexpected_ip} }, "$current_ip" unless $expected_ips =~ /(?:^|\s)$current_ip/;
          }

       return 1;
}

sub Send_alert {
        my ($subject,$msg) = @_;
#       $msg =~ s/\n/\<br\/\>\n/g;
        $msg = Wrap_msg($msg);
        if($send){
                #open(MAIL,"|$SENDMAIL -t") or die "Can\'t open sendmail: $!\n";
                #print MAIL "To: $recv_addr\n";
                #print MAIL "Subject: $subject\n";
                #print MAIL "Content-type:text/plain;charset=utf-8\n\n";
                #print MAIL "$msg\n";
                #close MAIL;
                open(TMP,">$tmp_file") or die "Can\'t create temp file: $tmp_file, $!\n";
                print TMP $msg;
                close TMP;
                system("$SENDMAIL -s $smtp_serv -xu $smtp_user -xp $smtp_pass -f $send_addr -t $recv_addr -u \'$subject\' -o message-charset=\'utf-8\' -o message-file=\'$tmp_file\'");
        }else{
                print "$subject\n$msg";
                exit;
        }
}

sub Wrap_msg {
        my $msg = shift;
        $msg =<<HTML;
$msg
HTML
        return $msg;

}


END {
        eval { unlink $tmp_file if -e $tmp_file; };
        die if $@;
}

